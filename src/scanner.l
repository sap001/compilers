%{
#include "parser.tab.h"
int keyword_differentiator(char *s);
%}

%%
[0-9][0-9]*	{  
	    //  printf("Token type: Number, Lexeme/Token Value: %s\n", yytext);  
			return NUMBER; 
                }
[a-zA-Z][a-zA-Z0-9]* {  
      
		  //	printf("Token type: Identifier, Lexeme/Token Value: %s\n", yytext);  
      return keyword_differentiator(yytext);	
      //	return IDENTIFIER; 
} 
 
[+\-\*/\^%]  {//printf("%c\n",yytext[0]); 
                return OP;}

[\<\>]    return RELOP1;
[\<\>=!]=  return RELOP2;

"&&"  return BOP;
"||"  return BOP;

\"([^\"]*)\" {
                //printf("string: %s\n",yytext);
                return STRING;
              }

"["   return '[';
"]"   return ']';
"{"		return '{';
"}"		return '}';
";"		return ';';
","		return ',';
":"		return ':';
"="   return  '=';
"!"   return  '!';
"("   return  '(';
")"   return  ')';

[ \t]		{ /* Do nothing */ }
\n      { yylineno++; }
.		{ 
		  printf("Unexpected token encountered: %s\n", yytext); 
		  return ETOK;
		}
%%
int keyword_differentiator(char *text){
 // printf("text: %s\n",text);
  if(!strcmp(text,"codeblock")) return  CODEBLOCK;
  else if(!strcmp(text,"declblock")) return  DECLBLOCK;
  else if(!strcmp(text,"else")) return ELSE;
  else if(!strcmp(text,"for")) return FOR;
  else if(!strcmp(text,"goto")) return GOTO;
  else if(!strcmp(text,"if")) return IF;
  else if(!strcmp(text,"print")|| !strcmp(yytext,"println")) return PRINT;
  else if(!strcmp(text,"read")) return READ;
  else if(!strcmp(text,"int")) { return TYPE; }
  else if(!strcmp(text,"bool")) { return TYPE; }
  else if(!strcmp(text,"true")) { return TRUE; }
  else if(!strcmp(text,"false")) { return FALSE; }
  else if(!strcmp(text,"while")) return WHILE;
  else return IDENTIFIER;
}
int yywrap(void){
  return 1;  
}
