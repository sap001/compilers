%{
  #include <stdio.h>
  #include <stdlib.h>
  FILE *yyin;
  extern int yylineno;
  extern int yylval;
  extern char *yytext;
  int yylex (void);
  void yyerror (char const *s);
%}

%token BOP
%token CODEBLOCK
%token DECLBLOCK
%token ELSE
%token ETOK
%token FALSE
%token FOR
%token GOTO
%token IDENTIFIER
%token IF
%token LABEL
%token NUMBER
%token OP
%token PRINT 
%token READ 
%token RELOP1
%token RELOP2
%token STRING
%token TYPE
%token TRUE
%token WHILE
%left '+'
%left '*'

%%

program:	decl_block code_block 
       ;

decl_block:  DECLBLOCK'{' declaration_list '}'
          ;
declaration_list: 
                | declaration ';' declaration_list 
                ;

declaration: TYPE IDENTIFIER  size_or_more_var
           ;

size_or_more_var:  
                | '[' NUMBER ']' size_or_more_var 
                | ',' IDENTIFIER size_or_more_var 
                ;

code_block:  CODEBLOCK'{' statement_list '}'
          ;

statement_list:
              | IDENTIFIER ':' statement_list
              | statement  statement_list 
              ;   
               
statement : assignment ';'
          | for_loop
          | goto_statement ';'
          | if_else
          | print_statement ';'
          | read_statement ';'
          | while_loop
          | ';' 
          ;

assignment : variable  '='  expression 
           ;

variable :  IDENTIFIER position
         ;

position :
         | '[' expression ']' position 
         ;

expression : term 
           | term OP expression 
           | '('expression')' OP expression 
           | '(' expression ')' 
           ;
           

term :  variable
     |  NUMBER
     ;
       

for_loop: FOR assignment ',' exp  increment '{' statement_list '}'
        ;

exp : expression
    | bool_exp
    ;

increment : 
          | ',' expression 
          ;

bool_exp : bool
         | '(' bool_exp ')' BOP bool_exp
         | '('  bool_exp ')'  
         | '!'  bool_exp   
         |  bool BOP bool_exp
         ;

bool :  TRUE 
     |  FALSE
     |  expression relop expression
     ;

relop : RELOP1
      | RELOP2
      ;

goto_statement: GOTO IDENTIFIER 
              | GOTO IDENTIFIER IF bool_exp
              ; 

if_else : IF '(' bool_exp ')' '{' statement_list '}'else        
        ;

else :
     | ELSE if_else
     | ELSE '{' statement_list '}'
     ;

print_statement: PRINT STRING  printstring
               | PRINT expression printstring
               ;

printstring :
            | ',' STRING printstring
            | ',' expression printstring
            ;

read_statement: READ expression readargs
              ;
readargs : 
         | ',' expression readargs
         ;

while_loop: WHILE '(' bool_exp ')' '{'statement_list '}'
          ;

/*
expr	: 	expr '+' expr 
	|	expr '*' expr 
	| 	NUMBER
	|	IDENTIFIER
	;
*/

%%

void yyerror (char const *s)
{
  if(yychar !=' ')
       fprintf (stderr, " At line %d: %s at \"%s\" before %c \n",yylineno,s,yytext,yychar);
  else
       fprintf (stderr, " At line %d: %s at \"%s\"\n",yylineno,s,yytext);
}

int main(int argc, char *argv[])
{
	if (argc == 1 ) {
		fprintf(stderr, "Correct usage: bcc filename\n");
		exit(1);
	}

	if (argc > 2) {
		fprintf(stderr, "Passing more arguments than necessary.\n");
		fprintf(stderr, "Correct usage: bcc filename\n");
	}

	yyin = fopen(argv[1], "r");

	yyparse();
}
